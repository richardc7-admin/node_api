# Api - Test en Node JS

Autor: Richard Camilo Saavedra


email: richardc7@misena.edu.co

## EndPoints:

## Registro Nuevo usuario - POST

http://fetest.mdigital.pro:2400/api/auth/signup
### Estructura
```json
{
    "username":"richardc784",
    "password": "password",
    "email": "rr@r.com",
    "age": "26",
    "civil_state": "single",
    "status":"true"
}
```

## Inicio de sesion - GET
http://fetest.mdigital.pro:2400/api/auth/login

### Estructura

```json
{
    "username": "richardc78",
    "password": "password"
}
```
## Listar usuarios - GET
Debe iniciar sesion para listar los usuarios.

http://fetest.mdigital.pro:2400/users

## Detelle de un usuario - GET

http://fetest.mdigital.pro:2400/users/{id}

## Actualizar un usuario - PUT
http://fetest.mdigital.pro:2400/users/{id}

## Eliminar un usuario - DELETE
http://fetest.mdigital.pro:2400/users/{id}



---
## Store Voto - POST

localhost:2400/votes/

### Estructura

``` json
{
    "character" : "603c6c6fce4a5e7e5cd92178",
    "user" : "603bf83629853e14e84990dd",
    "vote" : true
}
```
## Find Vote for user - POST

localhost:2400/votes/user/

### Estructura

``` json
{
    
    "id":"603bf83629853e14e84990dd"
}
```
## Find Votes for Character - POST

localhost:2400/votes/character

### Estructura

``` json
{
    
    "id":"603bf83629853e14e84990dd"
}
```