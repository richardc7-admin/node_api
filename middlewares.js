const jwt = require('jsonwebtoken');
const tokenSecret = "my-token-secret";
const User = require('./models/user');


exports.verify = (req, res, next) => {
    const token = req.headers.authorization
    if (!token) res.status(403).json({error: "please provide a token"})
    else {
        jwt.verify(token.split(" ")[1], tokenSecret, (err, value) => {
            if (err) res.status(500).json({error: 'failed to authenticate token'})
            req.user = value
            next()
        })
    }
}
exports.uniqueUser = (req, res, next) =>{
    const username = req.body.username
    User.findOne({ username: username }, (error, username) => {
        if(error) return res.status(500).send({ message: 'Error en el servidor' });
        if (username){
            res.status(400).json({ error: 'User already registered'});
        }else {
            next();
        }
    })
}