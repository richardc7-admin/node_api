const express = require("express")
const mongoose = require("mongoose")
var cors = require('cors')

const app = express()
const authRoute = require('./routes/auth')
const usersRoute = require('./routes/users')
const characterRoute = require('./routes/character')
const votesRoute = require('./routes/vote')

const  dbURI = "mongodb+srv://richardc7:43927913@cluster0.v0uyi.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

app.use(cors());
app.use(express.json())
app.use('/api/auth', authRoute)
app.use('/users', usersRoute)
app.use('/character', characterRoute)
app.use('/votes', votesRoute)

mongoose.connect(dbURI , {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection

db.on("error", (err)=>{console.error(err)})
db.once("open", () => {console.log("DB started successfully")})

app.listen(2400, () => {console.log("Server started: 2400")})