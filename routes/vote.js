const express = require('express')
const router = express.Router()
const Vote = require('../models/vote')
const middleware = require('../middlewares')

// Retornamos el total de los votos

router.get('/', middleware.verify, (req, res) => {
 
        Vote.find({}, (error, votes) => {
        if (error){
            res.status(500).json('Error retreving data');
        }else {
            res.status(200).json({
                ok: true,
                Votes: votes
            });
        }
    })

});

// crear un voto
router.post('/', middleware.verify, (req, res) => {
        const newVote =  Vote({
                character: req.body.character, 
                user: req.user.data._id, 
                vote: req.body.vote, 

            })
        newVote.save()
                .then(vote => {
                    res.status(200).json({vote: vote })
                })
                .catch(error => {
                    res.status(500).json(error)
                })
        

});

router.post('/user', middleware.verify, (req,res) => {
    let id = req.body.id;
    Vote.find({ user: id }, (err, voteData) =>{
        if(err){
            return res.status(400).json({
               ok: false,
               error: err.message  
            });
        }
        res.json({
            ok: true,
            user_votes: voteData,
            timestamps: voteData.createdAt
        });
    })
    
})
// Total votos usuario actual 

router.get('/actual-user', middleware.verify, (req,res) => {
    let id = req.user.data._id;
    Vote.find({ user: id }, (err, voteData) =>{
        if(err){
            return res.status(400).json({
               ok: false,
               error: err.message  
            });
        }
        res.json({
            ok: true,
            user_votes: voteData,
            timestamps: voteData.createdAt
        });
    })
    
})

router.post('/character', middleware.verify, (req,res) => {
    let id = req.body.id;
    Vote.find({ character: id }, (err, voteData) =>{
        if(err){
            return res.status(400).json({
               ok: false,
               error: err.message  
            });
        }
        res.json({
            ok: true,
            character_votes: voteData,
            timestamps: voteData.createdAt
        });
    })
    
})

module.exports = router;