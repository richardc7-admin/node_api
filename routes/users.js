const express = require('express')
const router = express.Router()
const User = require('../models/user')
const middleware = require('../middlewares')

// Listado de usuarios
router.get('/', middleware.verify, (req, res) => {
    User.find({}, (error, users) => {
        if (error){
            res.status(500).json('Error retreving data');
        }else {
            res.status(200).json({
                ok: true,
                Users: users
            });
        }
    })
    
});
// Detalles de usuario

router.get('/:id', middleware.verify, (req,res) => {
    let id = req.params.id;
    User.findById(id, (error, userData) =>{
        if(error){
            return res.status(400).json({
               ok: false,
               error: error  
            });
        }
        res.status(200).json({
            ok: true,
            usuario: userData
        });
    })
    

})
// Actualizar usuario

router.put('/:id', [middleware.verify, middleware.uniqueUser], (req,res) => {
    let id = req.params.id;
    let id_user_registered = req.user.data._id
    let body = req.body;
    User.findByIdAndUpdate(id, body, {new: true}, (error, userUpdated) => {
        if(error){
            return res.status(400).json({
               ok: false,
               error  
            });
        }
        res.json({
            ok: true,
            usuario: userUpdated,
            id_registerd: id_user_registered
        });
    })
})


// Eliminar/Desactivar usuario
router.delete('/:id', middleware.verify, (req, res) => {
    let id = req.params.id;
    User.findByIdAndRemove(id, (error, userDeleted) => {
        if(error) return res.status(500).send({ message: 'Error en el servidor' });
         
            if(userDeleted){
                return res.status(200).send({
                    nota: userDeleted
                });
            }else{
                return res.status(404).send({
                    message: 'No existe el usuario'
                });
            }

    })
    
});

module.exports = router;