const express = require('express')
const router = express.Router()
const User = require('../models/user')
const bcrypt = require('bcrypt')
const rounds = 10

const jwt = require('jsonwebtoken')
const tokenSecret = "my-token-secret"

const middleware = require('../middlewares')

router.post('/login', (req, res) => {
    User.findOne({username: req.body.username})
    .then(user => {
        if(!user) res.status(404).json({error: 'no user with that username found'})
        else {
            bcrypt.compare(req.body.password, user.password, (error, match) => {
                if (error) res.status(500).json(error)
                else if (match) res.status(200).json({token: generateToken(user), id: user.id })
                else res.status(403).json({error: 'passwords do not match'})
            })
        }
    })
    .catch(error => {
        res.status(500).json(error)
    })
});

router.post('/signup', middleware.uniqueUser, (req, res) => {
    bcrypt.hash(req.body.password, rounds, (error, hash) => {
        if (error) res.status(500).json(error)
        else {
            const newUser =  User({
                username: req.body.username, 
                password: hash, 
                age: req.body.age, 
                civil_state:req.body.civil_state,
                email: req.body.email
            })

            newUser.save()
                .then(user => {
                    res.status(200).json({token: generateToken(user), id: user.id })
                })
                .catch(error => {
                    res.status(500).json(error)
                })
        }
    })
});

// Actualizar usuario

router.get('/update/:id', middleware.verify , (req, res) => {
    
});


router.get('/jwt-test', middleware.verify , (req, res) => {
    res.status(200).json(req.user)
})

function generateToken(user){
    return jwt.sign({data: user}, tokenSecret, {expiresIn: '24h'})
}

module.exports = router
