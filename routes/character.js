const express = require('express')
const router = express.Router()
const Character = require('../models/character')
const middleware = require('../middlewares')

// Listar Personajes 

router.get('/',  (req, res)=>{
    Character.find({}, (err, chars) => {
        if(err){
            return res.status(400).json({
                ok: false,
                error: err  
            })
        }
        res.status(200).json({
            ok:true,
            character: chars
        
        })
    })
})

// Guardar personajes
router.post('/', middleware.verify, (req, res) => {
//    var character = new Character(req.body);
//    character.save();
    Character.create(req.body)
    .then(function(character) {
        res.send(character);
    });
});


router.post('/:id', middleware.verify, (req,res) => {
    let id = req.params.id;
    Character.findById(id, (err, charData) =>{
        if(err){
            return res.status(400).json({
               ok: false,
               error: err
            });
        }
        res.json({
            ok: true,
            character: charData,
            timestamps: charData.createdAt
        });
    })
    

})

module.exports = router;