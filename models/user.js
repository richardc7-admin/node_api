    const mongoose = require('mongoose');
    const uniqueValidator = require('mongoose-unique-validator');

    const model = mongoose.Schema({
        username: {
            type: String,
            required: [true, 'El username es requerido']
        },
        email: {
            type: String,
            required: [true, 'El email es requerido']
        },
        password: {
            type: String,
            required: [true, 'La contraseña es requerida']
        },
        age:{
            type: Number,
            required: false
        },
        civil_state: {
            type: String,
            required: false
        },
        status: {
            type: Boolean,
            default: true
        }

    });
    model.methods.toJSON = function(){
        let user = this;
        let userObject = user.toObject();
        delete userObject.password;
        return userObject;
    }
    
    model.plugin(uniqueValidator, {
        message: '{PATH} debe de ser único'
    });

module.exports = new mongoose.model("User", model)