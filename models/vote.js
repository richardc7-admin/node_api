    const mongoose = require('mongoose');
    const Schema = mongoose.Schema;
    const uniqueValidator = require('mongoose-unique-validator');

    const VoteSchema = new Schema({
        character: [
            { type: mongoose.Schema.Types.ObjectId,ref:'Character'}
        ],
        user: [
            { type: mongoose.Schema.Types.ObjectId,ref:'User'}
        ],
        vote: {
            type: Boolean,
            required: [true, 'El voto es requerido']
        },
    },
        { timestamps: true }
);

    const Vote = mongoose.model('Vote', VoteSchema);
    
module.exports = Vote;