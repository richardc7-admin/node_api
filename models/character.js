    const mongoose = require('mongoose');
    const Schema = mongoose.Schema;
    const uniqueValidator = require('mongoose-unique-validator');

    const CharacterSchema = new Schema({
        name: {
            type: String,
            required: [true, 'El username es requerido']
        },
        description: {
            type: String,
            required: [true, 'El email es requerido']
        },
        active:{
            type: Boolean,
            required: false
        },
        published_at:{
            type: Date,
            required: false
        }
        
    },
        { timestamps: true }
);

    const Character = mongoose.model('Character', CharacterSchema);
    
module.exports = Character;